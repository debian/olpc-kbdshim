/*
 * common.c -- special touchpad and keyboard support for XO:
 *  - mouse-based scrolling use the XO grab keys
 *  - touchpad and dpad rotation (to match screen rotation)
 *  - user activity monitoring
 *
 * Copyright (C) 2009,2010,2011, Paul G. Fox, inspired in places
 *  by code from mouseemu, by Colin Leroy and others.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see: http://www.gnu.org/licenses
 */

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

/* how many /dev/input/eventX devices to check */
#define EVENTDEVICES 20

#define MAXTIMERS 3
/* list of idle deltas.  last entry will always be zero */
extern int idledeltas[];

/* control commands will appear on this fd */
extern int fifo_fd;

extern char local_tpad_enabled[1024];

/* provided by common.c */
void report(const char *fmt, ...);
void dbg(int level, const char *fmt, ...);
void die(const char *fmt, ...);
void inject_uinput_event(unsigned int type, unsigned short code, int value);
void deinit_uinput_device(int);
void deinit_both_uinput_devices(void);
int setup_uinput(int);
int is_local_kbd(struct input_id *id);
int is_local_tpad(struct input_id *id);
void send_a_scroll(int x, int y);
void handle_dpad_pointer(struct input_event *ev);
int keyboard_event_worker(struct input_event *ev, int is_local);
void touchpad_event_worker(struct input_event *ev, int is_local);
void abs_touchpad_event_worker(struct input_event *ev, int is_local);
void touchscreen_event_worker(struct input_event *ev, int is_local);
void ebook_event_worker(struct input_event *ev);
void touchscreen_config_axis(int axis, struct input_absinfo *info);

void command_worker(char *command);
void send_event(char *e);
int init_fifo(char *fifo_node);
void deinit_fifo(void);
void sighandler(int sig);

/* called from common.c */
int setup_input(void);
void reinit_activity_monitor(void);
void monitor_input(void);
