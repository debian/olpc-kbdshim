# Copyright (C) 2009 Paul G. Fox
# Licensed under the terms of the GNU GPL v2 or later; see COPYING for details.


PACKAGE=olpc-kbdshim
VERSION=27

FDIST=fc15

# PGF_CROSS=1
ifneq ($(PGF_CROSS),)
    CC=arm-linux-gnueabi-gcc  
    UDEVLIB=arm-cross/libudev.so.0
    CFLAGS = -DPGF_CROSS
else
    UDEVLIB = -ludev 
endif

# don't edit .spec -- edit .spec.tmpl
SPEC=$(PACKAGE).spec

DATETAG=$(shell date +%Y%m%d)
GITHEAD=git$(shell test -d .git && git rev-parse --short HEAD )

ifeq ($(do_rel),)
    SNAP=.$(DATETAG)$(GITHEAD)
endif

RELEASE=$(shell cat .spec_release 2>/dev/null || echo error)
SRELEASE=$(RELEASE)$(SNAP)

TARBALL=$(PKGVER)-$(GITHEAD).tar.gz
SRPM=$(PKGVER)-$(SRELEASE).src.rpm

MOCK=./mock-wrapper -r fedora-14-i386 --resultdir=$(MOCKDIR)
MOCKDIR=./rpms
CWD=$(shell pwd)

PKGVER=$(PACKAGE)-$(VERSION)

# may be set externally to RPM_OPT_FLAGS
OPT_FLAGS ?= -O2 -g

#####

# PROGS = olpc-kbdshim olpc-kbdshim-hal olpc-kbdshim-udev
PROGS = olpc-kbdshim olpc-kbdshim-udev

all: $(PROGS)
# all: olpc-kbdshim-hal

# the "hal" version runs as a HAL addon daemon, and needs several
# libraries for the HAL interfaces
olpc-kbdshim-hal: \
	CFLAGS += -Wall $(OPT_FLAGS) -DVERSION=$(VERSION) \
	    $$(pkg-config --cflags hal) \
	    $$(pkg-config --cflags glib-2.0) \
	    $$(pkg-config --cflags dbus-glib-1)

olpc-kbdshim-hal: \
	LDLIBS += $$(pkg-config --libs hal) \
	    $$(pkg-config --libs glib-2.0) \
	    $$(pkg-config --libs dbus-glib-1)

olpc-kbdshim-hal: olpc-kbdshim-hal.o common.o
	$(CC) -o $@ olpc-kbdshim-hal.o common.o $(LDLIBS)
	
# the udev version relies only on libudev
olpc-kbdshim-udev: \
	CFLAGS += -Wall $(OPT_FLAGS) -DVERSION=$(VERSION)
	LDLIBS = $(UDEVLIB)

olpc-kbdshim-udev: olpc-kbdshim-udev.o common.o
	$(CC) -o $@ olpc-kbdshim-udev.o common.o $(LDLIBS)
	
# non-hal version of kbdshim needs nothing.  the downside is that
# it only monitors the local keyboard and touchpad, and won't detect
# user activity on USB devices.
olpc-kbdshim: CFLAGS += -Wall $(OPT_FLAGS) -DVERSION=$(VERSION)

olpc-kbdshim: olpc-kbdshim.o common.o common.h
	$(CC) -o $@ olpc-kbdshim.o common.o $(LDLIBS)

olpc-kbdshim.o olpc-kbdshim-hal.o common.o: common.h

#####

# testing targets
tarball:  $(TARBALL)
srpm: $(SRPM)

public_html: $(TARBALL) $(SRPM) rpms/$(PKGVER)-$(SRELEASE).$(FDIST).i686.rpm
	scp $(TARBALL) $(SRPM)  \
		crank:public_html/rpms/srpms
	scp rpms/$(PKGVER)-$(SRELEASE).$(FDIST).i686.rpm \
		crank:public_html/rpms

public_rpms: $(TARBALL) $(SRPM) rpms/$(PKGVER)-$(SRELEASE).$(FDIST).i686.rpm
	scp $(TARBALL) $(SRPM) rpms/$(PKGVER)-$(SRELEASE).$(FDIST).i686.rpm \
		crank:public_rpms/f14

privdist:
	scp rpms/$(PKGVER)-$(SRELEASE).$(FDIST).i686.rpm \
		crank:public_html/private_rpms
	ssh crank ln -sf \
		$(PKGVER)-$(SRELEASE).$(FDIST).i686.rpm \
		public_html/private_rpms/$(PKGVER)-$(RELEASE).latest.rpm

# create the real spec (carefully!) so it refers to a) our tarball, and
# b) our prerelease string.
$(SPEC): ALWAYS
	sed \
	-e 's/__VERSION__/$(VERSION)/' \
	-e 's/__RELEASE__/$(SRELEASE)/' \
	-e 's/__TARBALL__/$(TARBALL)/' \
	$(SPEC).tmpl > $(SPEC)


# build the tarball directly from git.
# THIS MEANS NO UNCOMMITED CHANGES WILL BE INCLUDED!!!

$(TARBALL):
	-git diff --exit-code # working copy is clean?
	-git diff --cached --exit-code # uncommitted changes?
	git archive --format=tar --prefix=$(PKGVER)/ HEAD | gzip -c > $@


# build the SRPM from the spec and the tarball

$(SRPM): $(SPEC) $(TARBALL)
	rpmbuild --define "_specdir $(CWD)" \
		 --define "_sourcedir $(CWD)" \
		 --define "_builddir $(CWD)"  \
		 --define "_srcrpmdir $(CWD)"  \
		 --define "_rpmdir $(CWD)"  \
		 --define "dist %nil"  \
		 --nodeps -bs $(SPEC)

# build rpm from the srpm
mock: $(SRPM)
	@mkdir -p $(MOCKDIR)
	$(MOCK) -q --init
	$(MOCK) --installdeps $(SRPM)
	$(MOCK) -v --no-clean --rebuild $(SRPM)

clean:
	rm -f *.o $(PROGS)
	-$(RM) $(SRPM) $(TARBALL)
	-$(RM) -rf $(MOCKDIR)

.PHONY: tarball srpm mock ALWAYS
